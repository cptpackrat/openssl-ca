@nfi/openssl-ca
======
[![npm version][npm-image]][npm-url]
[![pipeline status][pipeline-image]][pipeline-url]
[![coverage status][coverage-image]][coverage-url]
[![standard-js][standard-image]][standard-url]

Basic X.509 CA built around the OpenSSL command-line utility.

## Installation
```
npm install @nfi/openssl-ca
```

## Usage Example
```js
const { CA } = require('@nfi/openssl-ca')

/* Create a new self-signed CA. */
const ca = CA.generate({
  pass: 'password',
  bits: 2048,
  days: 3650,
  subj: [
    'CN=Test CA',
    'O=Test Organisation'
  ]
})

/* Issue a certificate from that CA. */
const cert = await ca.guard((ca) => {
  return ca.issue({
    pass: 'password',
    bits: 2048,
    days: 365,
    subj: [
      'CN=Test Server',
      'OU=Test Department',
      'O=Test Organisation'
    ]
    sans: [
      'DNS:server.test.org'
    ]
  })
})
```

[npm-image]: https://img.shields.io/npm/v/@nfi/openssl-ca.svg
[npm-url]: https://www.npmjs.com/package/@nfi/openssl-ca
[pipeline-image]: https://gitlab.com/cptpackrat/openssl-ca/badges/master/pipeline.svg
[pipeline-url]: https://gitlab.com/cptpackrat/openssl-ca/commits/master
[coverage-image]: https://gitlab.com/cptpackrat/openssl-ca/badges/master/coverage.svg
[coverage-url]: https://gitlab.com/cptpackrat/openssl-ca/commits/master
[standard-image]: https://img.shields.io/badge/code%20style-standard-brightgreen.svg
[standard-url]: http://standardjs.com/
