# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## [2.1.1](https://gitlab.com/cptpackrat/openssl-ca/compare/v2.1.0...v2.1.1) (2020-03-30)


### Housekeeping

* Fixed linter missing files; renamed script to lint. ([3c52b63](https://gitlab.com/cptpackrat/openssl-ca/commit/3c52b63d44aeab5d714eba365277d69cc1bd1cf8))
* Updated typescript target to ES2018. ([9a45a45](https://gitlab.com/cptpackrat/openssl-ca/commit/9a45a45e58f380b3a602950d1c978f1a0b39fd7d))
* Updated dependencies. ([001acbe](https://gitlab.com/cptpackrat/openssl-ca/commit/001acbe731e05b05b0cef6c1d4ea1280264c968a))
* Various CI changes. ([b0e2b6b](https://gitlab.com/cptpackrat/openssl-ca/commit/b0e2b6bb7790f7e6f3173a03be3bcc196cc50d1a))

## [2.1.0](https://gitlab.com/cptpackrat/openssl-ca/compare/v2.0.0...v2.1.0) (2020-01-15)


### Features

* Added option to assign a specific serial number to certificates. ([9b08b9c](https://gitlab.com/cptpackrat/openssl-ca/commit/9b08b9cc990aca48e63693e77986a341a42a5594))
* Changed starting serial number to be an optional parameter; random serials will assigned if it is ommitted. ([e30dc70](https://gitlab.com/cptpackrat/openssl-ca/commit/e30dc705267166ee646bbb514b4f09430b4b186b))


### Bug Fixes

* Fixed issueCA returning a CA object rather than just a CertPair. ([1c74e85](https://gitlab.com/cptpackrat/openssl-ca/commit/1c74e85166740a0aad3be513a265992000e12077))


### Housekeeping

* Switched linter to standardx for better TS support. ([6dae0dc](https://gitlab.com/cptpackrat/openssl-ca/commit/6dae0dcf1334c9bf68664b8bb084ab8ea773b264))

## [2.0.0](https://gitlab.com/cptpackrat/openssl-ca/compare/v1.0.1...v2.0.0) (2020-01-02)


### BREAKING CHANGES

* Certificates and private keys are now handled as buffers rather
than strings.
* By default, temporary files are no longer scrubbed
before deletion; all functions/methods that generate temporary files
now accept a boolean option to enable scrubbing.

### Features

* Added support for generating EC private keys. ([3b98719](https://gitlab.com/cptpackrat/openssl-ca/commit/3b98719dd11514f0c5e27bcbae5427e3975042d7))


### Housekeeping

* Updated dependencies. ([a32a77f](https://gitlab.com/cptpackrat/openssl-ca/commit/a32a77fd3a4ce477abe8d91853a9377680a716a0))


### Improvements

* Changed internal storage of certificates and private keys to use buffers. ([94c04ad](https://gitlab.com/cptpackrat/openssl-ca/commit/94c04adeefac5c7121be748e6a6c1ded9ef919a4))
* Changed temporary file scrubbing to be an optional feature. ([47af8d1](https://gitlab.com/cptpackrat/openssl-ca/commit/47af8d1eeb8d7e4ba80881eaf4c1f2e78f1c6041))

## [1.0.1](https://gitlab.com/cptpackrat/openssl-ca/compare/v1.0.0...v1.0.1) (2019-11-10)


### Improvements

* Updated dependencies; minor code changes to accomodate. ([58810b9](https://gitlab.com/cptpackrat/openssl-ca/commit/58810b9bbff6fd7fafc628625be75cb2b440b3fb))
