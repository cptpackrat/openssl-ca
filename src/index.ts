
/* global BigInt */

import { guard, TempFile } from '@nfi/safeish-tmp'
import { openssl } from '@nfi/openssl-cmd'
import { randomBytes } from 'crypto'

type CertType = 'ca' | 'ica' | 'cert'

export interface CertPair {
  /** PEM-formatted certificate. */
  cert: Buffer
  /** PEM-formatted private key. */
  pkey: Buffer
}

/** Common options for generating certificate pairs. */
interface Options {
  /** Desired private key size in bits. */
  bits?: number
  /** Passphrase to encrypt private key. */
  pass?: string
  /** Number of days the certificate should remain valid. */
  days?: number
  /** Subject DN entries to be included. */
  subj?: string[]
  /** Other X.509v3 extensions to be included. */
  exts?: { [index: string]: string }
  /** Desired EC curve name; implies switch from RSA to EC private key. */
  curve?: string
  /** Attempt to scrub any created temporary files before deleting them. */
  scrub?: boolean
  /** Serial number to assign to this certificate. */
  serial?: bigint
}

/** Options for generating end-entity certificate pairs. */
export interface CertOptions extends Options {
  /** Subject alternate names to be included. */
  sans?: string[]
}

/** Options for generating CA certificate pairs. */
export interface CAOptions extends Options {
  /** Maximum certificate path length. */
  plen?: number
}

/** Options for generating a self-signed CA object. */
export interface GenerateOptions extends CAOptions {
  /** Next serial number for issued certificates. */
  next?: bigint
}

export class CA {
  /** PEM-formatted CA certificate. */
  cert: Buffer
  /** PEM-formatted CA private key. */
  pkey: Buffer
  /** Next serial number for issued certificates. */
  next?: bigint
  /** Private key passphrase. */
  pass?: string

  constructor (cert: Buffer, pkey: Buffer, pass?: string)
  constructor (cert: Buffer, pkey: Buffer, next: bigint, pass?: string)

  /** @param cert PEM-formatted CA certificate.
    * @param pkey PEM-formatted CA private key.
    * @param next Next serial number for issued certificates.
    * @param pass Private key passphrase. */
  constructor (cert: Buffer, pkey: Buffer, next?: any, pass?: string) {
    this.cert = cert
    this.pkey = pkey
    if (typeof next === 'bigint') {
      this.next = next
      this.pass = pass
    } else {
      this.pass = next
    }
  }

  /** Temporarily write certificate and private key so they
    * can be used for CA operations by the provided callback,
    * then delete both files after it completes.
    * @param scrub Attempt to scrub any temporary files before deleting them.
    * @returns A promise resolving to the return value of the callback. */
  async guard<T> (scrub: boolean, callback: (ca: TempCA) => Promise<T> | T): Promise<T>

  /** Temporarily write certificate and private key so they
    * can be used for CA operations by the provided callback,
    * then delete both files after it completes.
    * @returns A promise resolving to the return value of the callback. */
  async guard<T> (callback: (ca: TempCA) => Promise<T> | T): Promise<T>

  async guard<T> (scrub: any, callback?: any): Promise<T> {
    if (!callback) {
      callback = scrub
      scrub = false
    }
    return guard(scrub, async (cert) => {
      return guard(scrub, async (pkey) => {
        await cert.write(this.cert)
        await pkey.write(this.pkey)
        return callback(new TempCA(this, cert, pkey))
      })
    })
  }

  /** Generate a new self-signed CA. */
  static generate (opts: GenerateOptions = {}): Promise<CA> {
    return guard(!!opts.scrub, async (extsFile) => {
      return guard(!!opts.scrub, async (pkeyFile) => {
        await genexts(extsFile, 'ca', opts)
        await genpkey(pkeyFile, opts)
        const pkey = (await pkeyFile.readAll())
        const cert = (await openssl('x509')
          .args('-req', '-sha256')
          .args('-extfile', extsFile.path)
          .args('-signkey', pkeyFile.path)
          .args('-set_serial', opts.serial ? opts.serial.toString() : '1')
          .argsIf(opts.days, '-days', `${opts.days}`)
          .argsIf(opts.pass, '-passin', `pass:${opts.pass}`)
          .stdin(await genreq(pkeyFile, opts))
          .exec()
        ).stdout
        return opts.next
          ? new CA(cert, pkey, opts.next, opts.pass)
          : new CA(cert, pkey, opts.pass)
      })
    })
  }
}

/** Represents a CA object whose certificate and private key
  * have temporarily been written so that it can be used to
  * issue certificates. */
export class TempCA {
  /** Parent CA object. */
  private ca: CA
  /** Temporary storage for CA certificate. */
  private certFile: TempFile
  /** Temporary storage for CA private key. */
  private pkeyFile: TempFile

  constructor (ca: CA, cert: TempFile, pkey: TempFile) {
    this.ca = ca
    this.certFile = cert
    this.pkeyFile = pkey
  }

  /** Issue an end-entity certificate from this CA. */
  issue (opts: CertOptions = {}) {
    return this._issue('cert', opts)
  }

  /** Issue an intermediate CA certificate from this CA. */
  issueCA (opts: CAOptions = {}) {
    return this._issue('ica', opts)
  }

  /** Issue a certificate from this CA. */
  private async _issue (type: CertType, opts: CertOptions & CAOptions): Promise<CertPair> {
    return guard(!!opts.scrub, async (extsFile) => {
      return guard(!!opts.scrub, async (pkeyFile) => {
        await genexts(extsFile, type, opts)
        await genpkey(pkeyFile, opts)
        return {
          pkey: (await pkeyFile.readAll()),
          cert: (await openssl('x509')
            .args('-req', '-sha256')
            .args('-extfile', extsFile.path)
            .args('-CAkey', this.pkeyFile.path)
            .args('-CA', this.certFile.path)
            .args('-set_serial', opts.serial
              ? opts.serial.toString()
              : this._issueSerial().toString())
            .argsIf(opts.days, '-days', `${opts.days}`)
            .argsIf(this.ca.pass, '-passin', `pass:${this.ca.pass}`)
            .stdin(await genreq(pkeyFile, opts))
            .exec()
          ).stdout
        }
      })
    })
  }

  private _issueSerial () {
    if (typeof this.ca.next === 'bigint') {
      return this.ca.next++
    }
    return BigInt('0x' + randomBytes(16).toString('hex'))
  }
}

/** Generate and return a certificate signing request.
  * @param pkey Temp file containing private key.
  * @param opts Additional options. */
async function genreq (pkey: TempFile, opts: {
  subj?: string[]
  pass?: string
}) {
  return (await openssl('req')
    .args('-new', '-batch')
    .args('-key', pkey.path)
    .argsIf(opts.subj, '-subj', `/${(opts.subj || []).join('/')}`)
    .argsIf(opts.pass, '-passin', `pass:${opts.pass}`)
    .exec()
  ).stdout
}

/** Generate a new private key and write it to the provided temp file.
  * @param dest Destination file.
  * @param opts Additional options. */
async function genpkey (dest: TempFile, opts: {
  bits?: number
  pass?: string
  curve?: string
}) {
  await openssl('genpkey')
    .args('-algorithm', opts.curve ? 'ec' : 'rsa')
    .args('-out', dest.path)
    .argsIf(opts.curve, '-pkeyopt', `ec_paramgen_curve:${opts.curve}`)
    .argsIf(opts.bits, '-pkeyopt', `rsa_keygen_bits:${opts.bits}`)
    .argsIf(opts.pass, '-pass', `pass:${opts.pass}`, '-aes256')
    .exec()
}

/** Generate a list of X509v3 extensions and write them to the provided temp file.
  * @param dest Destination file.
  * @param type Certificate type; root CA, intermediate CA, or end-entity.
  * @param opts Additional options. */
async function genexts (dest: TempFile, type: 'ca' | 'ica' | 'cert', opts: {
  exts?: { [index: string]: string }
  sans?: string[]
  plen?: number
}) {
  const exts: { [index: string]: string } = {
    subjectKeyIdentifier: 'hash',
    basicConstraints: type === 'cert'
      ? 'critical,CA:false'
      : 'plen' in opts
        ? `critical,CA:true,pathlen:${opts.plen}`
        : 'critical,CA:true',
    keyUsage: type === 'cert'
      ? 'critical,digitalSignature,keyEncipherment'
      : 'critical,keyCertSign,cRLSign'
  }
  if (type !== 'ca') {
    exts.authorityKeyIdentifier = 'keyid:always'
  }
  if (type === 'cert') {
    exts.extendedKeyUsage = 'serverAuth,clientAuth'
  }
  if (opts.sans) {
    exts.subjectAltName = opts.sans.join(',')
  }
  Object.assign(exts, opts.exts)
  await dest.write(Object
    .keys(exts)
    .map((name) => `${name}=${exts[name]}`)
    .join('\n'))
}
