
/* eslint-disable no-multi-spaces */
/* eslint-disable camelcase */
/* global BigInt */

import * as fs from 'fs'
import { command } from '@nfi/openssl-cmd'
import { expect } from 'chai'
import { CA, CertPair } from '../src'
import { TempFile } from '@nfi/safeish-tmp'
import { randomBytes } from 'crypto'

describe('openssl', function () {
  this.timeout(5000)
  describe('CA', () => {
    describe('generate', () => {
      itGeneratesCertPair('a new CA certificate pair', () => CA.generate())
      itAcceptsOption_PrivateKeySize((bits) => CA.generate({ bits: bits }))
      itAcceptsOption_PrivateKeyCurve((curve) => CA.generate({ curve: curve }))
      itAcceptsOption_PrivateKeyPassphrase((pass) => CA.generate({ pass: pass }))
      itAcceptsOption_SubjectDN((subj) => CA.generate({ subj: subj }))
      itAcceptsOption_ValidityPeriod((days) => CA.generate({ days: days }))
      itAcceptsOption_MaxPathLength((plen) => CA.generate({ plen: plen }))
      itAcceptsOption_Extensions((exts) => CA.generate({ exts: exts }))
      itAcceptsOption_SerialNumber((serial) => CA.generate({ serial: serial }))
      itAssignsFixedSerialNumber(BigInt(1), () => CA.generate())
      itMatchesInCertificateDump(() => CA.generate(), [
        ['inserts Subject Key Identifier',
          /X509v3 Subject Key Identifier:\s+(?:[0-9A-F]{2}:)+[0-9A-F]{2}/],
        ['inserts Basic Constraints',
          /X509v3 Basic Constraints:\s+critical\s+CA:TRUE/],
        ['inserts Key Usage',
          /X509v3 Key Usage:\s+critical\s+Certificate Sign, CRL Sign/]
      ])
    })
    describe('issue', () => {
      let ca: CA
      before(async () => {
        ca = await CA.generate({
          subj: ['CN=Test CA']
        })
      })
      itGeneratesCertPair('a new certficate pair', () => ca.guard((ca) => ca.issue()))
      itAcceptsOption_PrivateKeySize((bits) => ca.guard((ca) => ca.issue({ bits: bits })))
      itAcceptsOption_PrivateKeyCurve((curve) => ca.guard((ca) => ca.issue({ curve: curve })))
      itAcceptsOption_PrivateKeyPassphrase((pass) => ca.guard((ca) => ca.issue({ pass: pass })))
      itAcceptsOption_SubjectDN((subj) => ca.guard((ca) => ca.issue({ subj: subj })))
      itAcceptsOption_ValidityPeriod((days) => ca.guard((ca) => ca.issue({ days: days })))
      itAcceptsOption_SubjectAltName((sans) => ca.guard((ca) => ca.issue({ sans: sans })))
      itAcceptsOption_Extensions((exts) => ca.guard((ca) => ca.issue({ exts: exts })))
      itAcceptsOption_SerialNumber((serial) => ca.guard((ca) => ca.issue({ serial: serial })))
      itMatchesInCertificateDump(() => ca.guard((ca) => ca.issue()), [
        ['inserts Subject Key Identifier',
          /X509v3 Subject Key Identifier:\s+(?:[0-9A-F]{2}:)+[0-9A-F]{2}/],
        ['inserts Authority Key Identifier',
          /X509v3 Authority Key Identifier:\s+keyid(?::[0-9A-F]{2})+/],
        ['inserts Basic Constraints',
          /X509v3 Basic Constraints:\s+critical\s+CA:FALSE/],
        ['inserts Key Usage',
          /X509v3 Key Usage:\s+critical\s+Digital Signature, Key Encipherment/],
        ['inserts Extended Key Usage',
          /X509v3 Extended Key Usage:\s+TLS Web Server Authentication, TLS Web Client Authentication/]
      ])
      context('when a starting serial number is provided', () => {
        before(async () => {
          ca = await CA.generate({
            subj: ['CN=Test CA'],
            next: BigInt(1)
          })
        })
        itAssignsSequentialSerialNumbers(async () => {
          const next = ca.next!
          return {
            pair: await ca.guard((ca) => ca.issue()),
            next: next
          }
        })
      })
    })
    describe('issueCA', () => {
      let ca: CA
      before(async () => {
        ca = await CA.generate({
          subj: ['CN=Test CA']
        })
      })
      itGeneratesCertPair('a new CA certficate pair', () => ca.guard((ca) => ca.issueCA()))
      itAcceptsOption_PrivateKeySize((bits) => ca.guard((ca) => ca.issueCA({ bits: bits })))
      itAcceptsOption_PrivateKeyCurve((curve) => ca.guard((ca) => ca.issueCA({ curve: curve })))
      itAcceptsOption_PrivateKeyPassphrase((pass) => ca.guard((ca) => ca.issueCA({ pass: pass })))
      itAcceptsOption_SubjectDN((subj) => ca.guard((ca) => ca.issueCA({ subj: subj })))
      itAcceptsOption_ValidityPeriod((days) => ca.guard((ca) => ca.issueCA({ days: days })))
      itAcceptsOption_MaxPathLength((plen) => ca.guard((ca) => ca.issueCA({ plen: plen })))
      itAcceptsOption_SerialNumber((serial) => ca.guard((ca) => ca.issueCA({ serial: serial })))
      itMatchesInCertificateDump(() => ca.guard((ca) => ca.issueCA()), [
        ['inserts Subject Key Identifier',
          /X509v3 Subject Key Identifier:\s+(?:[0-9A-F]{2}:)+[0-9A-F]{2}/],
        ['inserts Authority Key Identifier',
          /X509v3 Authority Key Identifier:\s+keyid(?::[0-9A-F]{2})+/],
        ['inserts Basic Constraints',
          /X509v3 Basic Constraints:\s+critical\s+CA:TRUE/],
        ['inserts Key Usage',
          /X509v3 Key Usage:\s+critical\s+Certificate Sign, CRL Sign/]
      ])
      context('when a starting serial number is provided', () => {
        before(async () => {
          ca = await CA.generate({
            subj: ['CN=Test CA'],
            next: BigInt(1)
          })
        })
        itAssignsSequentialSerialNumbers(async () => {
          const next = ca.next!
          return {
            pair: await ca.guard((ca) => ca.issueCA()),
            next: next
          }
        })
      })
    })
    describe('guard', () => {
      let ca: CA
      before(async () => {
        ca = await CA.generate({
          subj: ['CN=Test CA']
        })
      })
      it('deletes temporary files after completion', async () => {
        let tca: any
        await ca.guard((ca: any) => {
          tca = ca
          assertOpen(tca.certFile)
          assertOpen(tca.pkeyFile)
        })
        assertClosed(tca.certFile)
        assertClosed(tca.pkeyFile)
      })
      it('tries to scrub temporary files if requested', async () => {
        let tca: any
        let pfd: any
        let cfd: any
        await ca.guard(true, (ca: any) => {
          tca = ca
          assertOpen(tca.certFile)
          assertOpen(tca.pkeyFile)
          cfd = fs.openSync(tca.certFile.path, 'r')
          pfd = fs.openSync(tca.pkeyFile.path, 'r')
        })
        assertClosed(tca.certFile)
        assertClosed(tca.pkeyFile)
        assertScrubbed(cfd, ca.cert)
        assertScrubbed(pfd, ca.pkey)
      })
    })
  })
})

function itGeneratesCertPair (desc: string, fn: () => Promise<CertPair>) {
  it(`generates ${desc}`, async () => {
    const pair = await fn()
    expect(pair.pkey).to.match(pemex('PRIVATE KEY'))
    expect(pair.cert).to.match(pemex('CERTIFICATE'))
  })
}

function itAcceptsOption_PrivateKeySize (fn: (bits: number) => Promise<CertPair>) {
  it('accepts private key size', async () => {
    for (const bits of [1024, 2048, 4096]) {
      const pair = await fn(bits)
      const dump = await dumppkey(pair.pkey)
      expect(dump).to.contain(`Private-Key: (${bits} bit`)
    }
  })
}

function itAcceptsOption_PrivateKeyCurve (fn: (curve: string) => Promise<CertPair>) {
  it('accepts private key curve', async () => {
    for (const curve of ['secp256k1', 'secp384r1', 'secp521r1']) {
      const pair = await fn(curve)
      const dump = await dumppkey(pair.pkey)
      expect(dump).to.contain(`ASN1 OID: ${curve}`)
    }
  })
}

function itAcceptsOption_PrivateKeyPassphrase (fn: (pass: string) => Promise<CertPair>) {
  it('accepts private key passphrase', async () => {
    const pair = await fn('password')
    expect(pair.pkey).to.match(pemex('ENCRYPTED PRIVATE KEY'))
    const dump = await dumppkey(pair.pkey, 'password')
    expect(dump).to.match(pemex('PRIVATE KEY'))
  })
}

function itAcceptsOption_SubjectDN (fn: (subj: string[]) => Promise<CertPair>) {
  it('accepts subject DN entries', async () => {
    const names = [
      ['CN', 'Test Name'],
      ['OU', 'Test Unit'],
      ['O',  'Test Organisation'],
      ['L',  'Test Location'],
      ['ST', 'TST'],
      ['C',  'TC']
    ]
    const pair = await fn(names.map((name) => name.join('=')))
    const dump = await dumpcert(pair.cert)
    for (const name of names) {
      expect(dump).to.contain(name.join(' = '))
    }
  })
}

function itAcceptsOption_ValidityPeriod (fn: (days: number) => Promise<CertPair>) {
  it('accepts validity period', async () => {
    for (const days of [30, 1000, 3650]) {
      const pair = await fn(days)
      const dump = await dumpcert(pair.cert)
      chkdate(mkdate(dump, /Not Before: (.+)\n/), 0, 5000)
      chkdate(mkdate(dump, /Not After : (.+)\n/), days * 86400000, 5000)
    }
  })
}

function itAcceptsOption_MaxPathLength (fn: (plen: number) => Promise<CertPair>) {
  it('accepts max path length', async () => {
    for (const plen of [0, 1, 5]) {
      const pair = await fn(plen)
      const dump = await dumpcert(pair.cert)
      expect(dump).to.contain(`CA:TRUE, pathlen:${plen}`)
    }
  })
}

function itAcceptsOption_SerialNumber (fn: (serial: bigint) => Promise<CertPair>) {
  it('accepts serial number', async () => {
    for (const serial of [BigInt(1), BigInt(12345678), randomBytes(8).readBigUInt64BE()]) {
      const pair = await fn(serial)
      const dump = await dumpserial(pair.cert)
      expect(dump).to.equal(serial)
    }
  })
}

function itAcceptsOption_SubjectAltName (fn: (sans: string[]) => Promise<CertPair>) {
  it('accepts SAN entries', async () => {
    const sans = [
      'DNS:foo.com',
      'DNS:*.bar.com',
      'IP:1.2.3.4'
    ]
    const pair = await fn(sans)
    const dump = await dumpcert(pair.cert)
    for (const str of [
      'X509v3 Subject Alternative Name',
      'DNS:foo.com',
      'DNS:*.bar.com',
      'IP Address:1.2.3.4'
    ]) {
      expect(dump).to.contain(str)
    }
  })
}

function itAcceptsOption_Extensions (fn: (exts: { [index: string]: string }) => Promise<CertPair>) {
  it('accepts X509v3 extensions', async () => {
    const exts = {
      basicConstraints: 'critical,CA:true,pathlen:5',
      subjectAltName: 'DNS:foo.com,IP:1.2.3.4',
      keyUsage: 'keyCertSign,cRLSign,digitalSignature'
    }
    const pair = await fn(exts)
    const dump = await dumpcert(pair.cert)
    for (const regex of [
      /X509v3 Basic Constraints:\s+critical\s+CA:TRUE,\s+pathlen:5/,
      /X509v3 Subject Alternative Name:\s+DNS:foo.com,\s+IP Address:1.2.3.4/,
      /X509v3 Key Usage:\s+Digital Signature, Certificate Sign, CRL Sign/
    ]) {
      expect(dump).to.match(regex)
    }
  })
}

function itAssignsFixedSerialNumber (next: bigint, fn: () => Promise<CertPair>) {
  it('assigns fixed serial number', async () => {
    const pair = await fn()
    const serial = await dumpserial(pair.cert)
    expect(serial).to.equal(next)
  })
}

function itAssignsSequentialSerialNumbers (fn: () => Promise<{ pair: CertPair, next: bigint }>) {
  it('assigns sequential serial numbers', async () => {
    for (let i = 0; i < 3; i++) {
      const { pair, next } = await fn()
      const serial = await dumpserial(pair.cert)
      expect(serial).to.equal(next)
    }
  })
}

function itMatchesInCertificateDump (fn: () => Promise<CertPair>, tests: [ string, RegExp ][]) {
  for (const test of tests) {
    it(test[0], async () => {
      const pair = await fn()
      const dump = await dumpcert(pair.cert)
      expect(dump).to.match(test[1])
    })
  }
}

export async function dumppkey (pkey: Buffer, pass?: string) {
  return (await command('pkey')
    .args('-text')
    .argsIf(pass, '-passin', `pass:${pass}`)
    .stdin(pkey)
    .exec()
  ).stdout.toString()
}

export async function dumpcert (cert: Buffer) {
  return (await command('x509')
    .args('-text', '-noout')
    .stdin(cert)
    .exec()
  ).stdout.toString()
}

export async function dumpserial (cert: Buffer) {
  return BigInt('0x' + (await command('x509')
    .args('-serial', '-noout')
    .stdin(cert)
    .exec()
  ).stdout.slice(7).toString())
}

export function pemex (type: string) {
  return new RegExp(`-----BEGIN ${type}-----\\n[a-zA-Z0-9/\\+\\n]+=*\\n-----END ${type}-----`)
}

export function mkdate (dump: string, regex: RegExp, capture: number = 1) {
  expect(dump).to.match(regex)
  return new Date((dump.match(regex) as string[])[capture])
}

export function chkdate (date: Date, offset: number, margin: number) {
  const target = Date.now() + offset
  expect(date.getTime()).to.be.within(target - margin, target + margin)
}

function assertOpen (file: TempFile) {
  fs.fstatSync(file.fd)
  fs.accessSync(file.path)
}

function assertClosed (file: TempFile) {
  expect(() => fs.fstatSync(file.fd)).to.throw(/EBADF/)
  expect(() => fs.accessSync(file.path)).to.throw(/ENOENT/)
}

function assertScrubbed (fd: number, data: Buffer) {
  const buf = Buffer.alloc(data.length)
  fs.readSync(fd, buf, 0, data.length, 0)
  fs.closeSync(fd)
  expect(buf).to.not.deep.equal(data)
  expect(buf).to.deep.equal(Buffer.alloc(data.length, 0))
}
